/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/gpu/gpu.hpp>
/* Header for class com_example_sudokusolver_camerautil_NativeFrameClass */

#ifndef _Included_com_example_sudokusolver_camerautil_NativeFrameClass
#define _Included_com_example_sudokusolver_camerautil_NativeFrameClass
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     com_example_sudokusolver_camerautil_NativeFrameClass
 * Method:    processFrame
 * Signature: (JJ)I
 */
JNIEXPORT jint JNICALL Java_com_example_sudokusolver_camerautil_NativeFrameClass_processFrame
  (JNIEnv *, jclass, jlong, jlong);

#ifdef __cplusplus
}
#endif
#endif
