#include <com_example_sudokusolver_camerautil_OpenCvNativeClass.h>

/*
    public static Mat connectComponents(Mat srcImage) {
        Size kernelSize = new Size(5, 5);
        Mat element = Imgproc.getStructuringElement(MORPH_RECT, kernelSize,
                new org.opencv.core.Point(1,1));
        Mat destImage = new Mat();
        Imgproc.morphologyEx(srcImage, destImage, Imgproc.MORPH_CLOSE, element);
        return destImage;
    }
    public static Mat detectEdges(Mat srcImage) {
        Imgproc.GaussianBlur(srcImage, srcImage, new Size(9,9), 0);
        Mat thresholdedImage = new Mat();
        double upperThreshold = Imgproc.threshold(srcImage, thresholdedImage, 0, 255,
                Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);
        double lowerThreshold = 0.1 * upperThreshold;
        return EdgeDetectors.Canny(srcImage, (int) lowerThreshold, (int) upperThreshold);
    }

grayImage = new Mat();
Imgproc.cvtColor(originalImage, originalImage, Imgproc.COLOR_BGR2RGB);
Imgproc.cvtColor(originalImage, grayImage, Imgproc.COLOR_BGR2GRAY);
Mat edgesImage = ImageProcessingUtil.detectEdges(grayImage);
Mat connectedEdgesImage = ImageProcessingUtil.connectComponents(edgesImage);
List<MatOfPoint> contours = ImageProcessingUtil.findContours(connectedEdgesImage, false,
        false);
if (contours.size() == 0) {
    return inputFrame.rgba();
}
MatOfPoint outerContour = contours.get(0);
MatOfPoint2f approxContourNew = ImageProcessingUtil.approximateContour(outerContour, true);
*/

JNIEXPORT jint JNICALL Java_com_example_sudokusolver_camerautil_OpenCvNativeClass_convertGray
  (JNIEnv *, jclass, jlong gridAddr, jlong clearAddr) {
    Mat& mGridCpu = *(Mat*)gridAddr;
    Mat& mClearCpu = *(Mat*)clearAddr;
    Mat mBlurCpu;
    Mat mThrCpu;
    GaussianBlur(mGridCpu, mBlurCpu, Size(9, 9), 0);
    adaptiveThreshold(mBlurCpu, mThrCpu, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 11, 2);
    int conv = 1;
    bitwise_not(mThrCpu, mClearCpu);
    jint returnValue;
    returnValue = (jint)conv;
  }
