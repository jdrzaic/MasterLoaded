LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
OPENCV_CAMERA_MODULES := on
OPENCV_INSTALL_MODULES := on
INSTALL_CUDA_LIBRARIES:=on
OPENCV_LIB_TYPE:=SHARED
include /Users/jelenadrzaic/NVPACK/OpenCV-2.4.8.2-Tegra-sdk/sdk/native/jni/OpenCV-tegra3.mk

LOCAL_SRC_FILES := com_example_sudokusolver_camerautil_OpenCvNativeClass.cpp,com_example_sudokusolver_camerautil_NativeFrameClass.cpp

LOCAL_LDLIBS += -llog
LOCAL_MODULE := MyLibs

include $(BUILD_SHARED_LIBRARY)