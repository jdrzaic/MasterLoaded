package com.example.sudokusolver.gameutil;
import android.content.Context;

import com.example.sudokusolver.views.buttonsgrid.NumberButton;
import com.example.sudokusolver.views.buttonsgrid.NumbersGrid;
import com.example.sudokusolver.views.sudokugrid.GameGrid;

import org.opencv.core.Point;

public class GameEngine {
    private static GameEngine instance;

    private GameGrid grid = null;
    private NumbersGrid numGrid = null;

    private int selectedPosX = -1, selectedPosY = -1;

    private GameEngine(){}

    public static GameEngine getInstance(){
        if( instance == null ){
            instance = new GameEngine();
        }
        instance.createNumGrid();
        return instance;
    }

    public static GameEngine getNewInstance() {
        instance = new GameEngine();
        instance.createNumGrid();
        return instance;
    }

    public void createGrid(Context context, int[][] sudoku) {
        grid = new GameGrid(context);
        grid.setGrid(sudoku);
    }

    private void createNumGrid() {
        if (numGrid == null) {
            numGrid = new NumbersGrid(new NumberButton[9]);
        }
    }

    public void createGrid(Context context) {
        createGrid(context, new int[9][9]);
    }

    public GameGrid getGrid(){
        return grid;
    }

    public NumbersGrid getNumGrid() {
        return numGrid;
    }

    public void setSelectedPosition( int x , int y ){
        numGrid.resetDisabled();

        selectedPosX = x;
        selectedPosY = y;
        // deselecting buttons
        if (selectedPosX < 0 || selectedPosY < 0) {
            return;
        }
        int[][] sudokuNumbers = new int[9][];
        for(int i = 0; i < 9; ++i) {
            sudokuNumbers[i] = grid.getRawValues()[i].clone();
        }
        for (int num = 0; num < 9; ++num) {
            sudokuNumbers[x][y] = num + 1;
            if (SudokuGenerator.getInstance().checkConflict(sudokuNumbers, y * 9 + x, num + 1)) {
                numGrid.setDisabled(num + 1);
            }
        }
    }

    public Point getSelectedPosition() {
        return new Point(selectedPosX, selectedPosY);
    }

    public void setNumber( int number ){
        if( selectedPosX != -1 && selectedPosY != -1 ){
            grid.setItem(selectedPosX,selectedPosY,number);
        }
    }
}