package com.example.sudokusolver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.sudokusolver.camerautil.IntentHelper;
import com.example.sudokusolver.imageprocessing.ImageProcessingUtil;
import com.example.sudokusolver.ocrutil.DigitRecognizer;
import com.googlecode.tesseract.android.TessBaseAPI;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SolverActivity extends Activity {

    private static String TAG = "darkroom::SolverActivity";
    private static final String lang = "eng";

    private static final String DATA_PATH = Environment.getExternalStorageDirectory().toString() + "/TesseractSample/";
    private static final String MNISTDATA_PATH = Environment.getExternalStorageDirectory().toString() + "/MNISTSample/";
    private static final String TESSDATA = "tessdata";
    private static final String MNISTDATA = "mnistdata";

    private ImageView sudokuView;
    Mat gridImage;
    private TessBaseAPI tessBaseApi;
    DigitRecognizer recognizer;
    private int[][] sudokuNumbers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solver);
        sudokuView = findViewById(R.id.SolverImageView);
        Object gridValue = IntentHelper.getObjectForKey("extractedGrid");
        Object originalValue = IntentHelper.getObjectForKey("originalImage");
        if (gridValue != null && originalValue != null) {
            recognizer = new DigitRecognizer();
            recognizer.readMNISTData(MNISTDATA_PATH.concat(MNISTDATA + "/train-images-idx3-ubyte"),
                    MNISTDATA_PATH.concat(MNISTDATA + "/train-labels-idx1-ubyte"));
            gridImage = (Mat) gridValue;
            Mat originalImage = (Mat) originalValue;
            Mat thresholdedImage = new Mat();
            // returns upperThreshold, ignoring it.
            Imgproc.threshold(gridImage, thresholdedImage, 0, 255,
                    Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);
            Imgproc.resize(gridImage, gridImage, new Size(252, 252), 0, 0, Imgproc.INTER_AREA);
            Core.bitwise_not(thresholdedImage, thresholdedImage);
            ImageProcessingUtil.displayImage(originalImage, sudokuView);
            // allow corrections
            prepareMNIST();
            sudokuNumbers = splitGrid(thresholdedImage);
        }
    }

    int[][] splitGrid(Mat grid) {
        // numbers in sudoku grid, if nothing for i,j, -1.
        int numbers[][] = new int[9][9];
        // Size of each number blob.
        int numberWidth = grid.cols() / 9;
        int numberHeight = grid.rows() / 9;
        for (int i = 0; i < 9; ++i) {
            for (int j = 0; j < 9; ++j) {
                Mat numberMat = new Mat(grid, new Rect(i * numberWidth, j * numberHeight, numberWidth, numberHeight));
                double result = extractTextMNIST(numberMat);
                numbers[i][j] = (int) result;
                Log.d(TAG, "Coordinate (" + i + ", " + j + ")");
                Log.d(TAG,"Parsed number equals: " + result);
            }
        }
        return numbers;
    }

    private double extractTextMNIST(Mat number) {
        return recognizer.findMatch(number, this);
    }

    /**
     * Prepare directory on external storage
     *
     * @param path
     * @throws Exception
     */
    private void prepareDirectory(String path) {

        File dir = new File(path);
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                Log.e(TAG, "ERROR: Creation of directory " + path + " failed, check does Android Manifest have permission to write to external storage.");
            }
        } else {
            Log.i(TAG, "Created directory " + path);
        }
    }

    /**
     * Copy tessdata files (located on assets/tessdata) to destination directory
     *
     * @param path - name of directory with .traineddata files
     */
    private void copyMNISTDataFiles(String path) {
        try {
            String fileList[] = getAssets().list(path);

            for (String fileName : fileList) {

                // open file within the assets folder
                // if it is not already there copy it to the sdcard
                String pathToDataFile = MNISTDATA_PATH + path + "/" + fileName;
                Log.i(TAG, "Path to a copied file: " + pathToDataFile);
                if (!(new File(pathToDataFile)).exists()) {

                    InputStream in = getAssets().open(path + "/" + fileName);

                    OutputStream out = new FileOutputStream(pathToDataFile);

                    // Transfer bytes from in to out
                    byte[] buf = new byte[1024];
                    int len;

                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();

                    Log.d(TAG, "Copied " + fileName + "to mnistdata");
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "Unable to copy files to mnistdata " + e.toString());
        }
    }

    private void prepareMNIST() {
        try {
            prepareDirectory(MNISTDATA_PATH + MNISTDATA);
        } catch (Exception e) {
            e.printStackTrace();
        }
        copyMNISTDataFiles(MNISTDATA);
    }

    // Create background task for digits recognition and sudoku solving
    public void solveManually(View view) {
        startSudokuViewActivity();
    }

    public void solveAutomatically(View view) {
        // todo: solve the sudoku
        startSudokuViewActivity();
    }

    private void startSudokuViewActivity() {
        Intent i = new Intent(this, AutomaticSolverActivity.class);
        Bundle mBundle = new Bundle();
        mBundle.putSerializable("sudokuNumbers", sudokuNumbers);
        i.putExtras(mBundle);
        startActivity(i);
    }
}
