package com.example.sudokusolver;

import java.io.IOException;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.View;

import com.example.sudokusolver.camerautil.IntentHelper;
import com.example.sudokusolver.camerautil.OpenCvNativeClass;
import com.example.sudokusolver.fileutil.FileUtils;
import com.example.sudokusolver.gameutil.GameEngine;
import com.example.sudokusolver.gameutil.SudokuGenerator;
import com.example.sudokusolver.imageprocessing.ImageProcessingUtil;

public class IODarkRoom extends Activity {

	private static final String TAG = "::DarkRoom::Activity";
	private static final int SELECT_PICTURE = 1;
	private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 5;
	
	private String selectedImagePath;
	// Original image loaded from the memory.
	private Mat originalImage;
	// Rescaled image.
	private Mat sampledImage;
	private Mat grayscaleImage;
	
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {

        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_iodark_room);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
		 && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
				!= PackageManager.PERMISSION_GRANTED) {

			requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
					MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
		}

	}

    private void handlePhotoIntent() {
	    Intent intent = new Intent(this, CameraShotActivity.class);
	    startActivity(intent);
    }

    private void handleGalleryOpening() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType("image/*");
		startActivityForResult(Intent.createChooser(
				intent, "Select picture"), SELECT_PICTURE);
	}

	@Override
    protected void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
    	OpenCVLoader.initAsync(
    			OpenCVLoader.OPENCV_VERSION_2_4_8, this, mLoaderCallback);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if (requestCode == SELECT_PICTURE && resultCode == Activity.RESULT_OK) {
	        Uri selectedImageUri = null;
	        if (data != null) {
	        	selectedImageUri = data.getData();
	    		selectedImagePath = getPath(selectedImageUri);
	            Log.i(TAG, "Selected image path: " + selectedImagePath);
	            // Initializes grayscaleImage and sampledImage
	    		loadImage(selectedImagePath);
	    		Mat edgesImage = ImageProcessingUtil.detectEdges(sampledImage);
	    		Mat connectedEdgesImage = ImageProcessingUtil.connectComponents(edgesImage);
	    		// Get biggest contour
	    		List<MatOfPoint> contours = ImageProcessingUtil.findContours(connectedEdgesImage,false,
                        false);
                // continue
                MatOfPoint outerContour = contours.get(0);
                MatOfPoint2f approxContour = ImageProcessingUtil.approximateContour(outerContour, true);
	    		ImageProcessingUtil.drawApproxContour(originalImage, approxContour);
                Mat correctedImage = ImageProcessingUtil.adjustPerspective(approxContour, grayscaleImage);
	    		if (correctedImage != null) {
					IntentHelper.addObjectForKey(correctedImage, "extractedGrid");
                    IntentHelper.addObjectForKey(originalImage, "originalImage");
                    Intent intent = new Intent(this, TensorflowSolverActivity.class);
					startActivity(intent);
	    		}
	        }
	    }
    }

	private String getPath(Uri uri) {
    	if (uri == null) {
    		return null;
    	}
    	return FileUtils.getPath(this, uri);
    }

    private void loadImage(String path) {
    	if (path == null) {
    		Log.e(TAG, "Unable to load image, path is null");
    		return;
    	}
    	originalImage = Highgui.imread(path);
    	Mat rgbImage = new Mat();
    	Mat grayImage = new Mat();
        Imgproc.cvtColor(originalImage, rgbImage, Imgproc.COLOR_BGR2RGB);
        Imgproc.cvtColor(rgbImage, grayImage, Imgproc.COLOR_BGR2GRAY);
        Display display = getWindowManager().getDefaultDisplay();
    	Point size = new Point();
    	display.getSize(size);
    	int imageWidth = size.x;
    	int imageHeight = size.y;
    	sampledImage = new Mat();
    	grayscaleImage = new Mat();
    	double downSampleRatio = ImageProcessingUtil.calculateSampledSize(
    			grayImage, imageWidth, imageHeight);
        Imgproc.resize(grayImage, sampledImage, new Size(),
                downSampleRatio, downSampleRatio, Imgproc.INTER_AREA);
        Imgproc.resize(rgbImage, rgbImage, new Size(),
                downSampleRatio, downSampleRatio, Imgproc.INTER_AREA);
    	try {
    		ExifInterface exif = new ExifInterface(selectedImagePath);
    		int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
    		switch (orientation) {
    		case ExifInterface.ORIENTATION_ROTATE_90:
    			// Mirror the image and flip on the y-axis
    			sampledImage = sampledImage.t();
    			Core.flip(sampledImage, sampledImage, 1);
                rgbImage = rgbImage.t();
                Core.flip(rgbImage, rgbImage, 1);
    			break;
    		case ExifInterface.ORIENTATION_ROTATE_270:
    			sampledImage = sampledImage.t();
    			Core.flip(sampledImage, sampledImage, 0);
                rgbImage = rgbImage.t();
                Core.flip(rgbImage, rgbImage, 0);
    			break;
    		case ExifInterface.ORIENTATION_ROTATE_180:
    			// TODO handle 180 degrees rotation
    			break;
    		}
    	} catch (IOException ex) {
    		Log.e(TAG, "Error loading the image.");
    		ex.printStackTrace();
    	}
    	grayscaleImage = sampledImage.clone();
    	originalImage = rgbImage;
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    handleGalleryOpening();
                }
                break;
            default:
                break;
        }
    }

    public void openGallery(View view) {
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        } else {
            handleGalleryOpening();
        }
    }

    public void takePhoto(View view) {
        handlePhotoIntent();
    }

    public void generateSudoku(View view) {
        int[][] numbersGrid = SudokuGenerator.getInstance().generateGrid();
        numbersGrid = SudokuGenerator.getInstance().removeElements(numbersGrid, 45);
        Intent i = new Intent(this, AutomaticSolverActivity.class);
        Bundle mBundle = new Bundle();
        mBundle.putSerializable("sudokuNumbers", numbersGrid);
        i.putExtras(mBundle);
        startActivity(i);
    }
}
