package com.example.sudokusolver;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.sudokusolver.camerautil.IntentHelper;
import com.example.sudokusolver.camerautil.OpenCvNativeClass;
import com.example.sudokusolver.imageprocessing.ImageProcessingUtil;
import com.example.sudokusolver.ocrutil.TensorFlowDigitRecognizer;
import com.example.sudokusolver.ocrutil.tensorflow.Classifier;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.List;

public class TensorflowSolverActivity extends Activity {

    private static String TAG = "darkroom::SolverActivity";
    private static int MIN_NUMBER_WIDTH = 10;
    private static int MIN_NUMBER_HEIGHT = 10;
    private static int PIXEL_THRESHOLD = 150;

    Mat gridImage;
    Bitmap bitMap;
    int[][] sudokuNumbers;

    static {
        System.loadLibrary("MyLibs");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tensorflow_solver);
        ImageView sudokuView = findViewById(R.id.TFSolverImageView);
        Object gridValue = IntentHelper.getObjectForKey("extractedGrid");
        Object originalValue = IntentHelper.getObjectForKey("originalImage");
        if (gridValue != null && originalValue != null) {
            gridImage = (Mat) gridValue;
            final Mat originalImage = (Mat) originalValue;
            ImageProcessingUtil.displayImage(originalImage, sudokuView);
            new ProcessDigitsTask().execute(gridImage);
            bitMap = Bitmap.createBitmap(
                    originalImage.cols(), originalImage.rows(), Bitmap.Config.RGB_565);
            Utils.matToBitmap(originalImage, bitMap);
        }
    }

    int[][] splitGrid(Mat grid, TensorFlowDigitRecognizer recognizer) {
        // numbers in sudoku grid, if nothing for i,j, -1.
        int numbers[][] = new int[9][9];
        // Size of each number blob.
        int numberWidth = grid.cols() / 9;
        int numberHeight = grid.rows() / 9;
        int reqi = 9;
        int reqj = 9;
        for (int i = 0; i < 9; ++i) {
            boolean stopped = false;
            for (int j = 0; j < 9; ++j) {
                Mat numberMat = new Mat(grid, new Rect(i * numberWidth, j * numberHeight, numberWidth, numberHeight));
                double result = findMatch(numberMat, recognizer);
                numbers[i][j] = (int) result;
                Log.d(TAG, "Coordinate (" + i + ", " + j + ")");
                Log.d(TAG,"Parsed number equals: " + result);
                if (reqi == i && reqj == j) {
                    stopped = true;
                    break;
                }
            }
            if (stopped) {
                break;
            }
        }
        return numbers;
    }

    public double findMatch(Mat testImage, TensorFlowDigitRecognizer recognizer) {
        Imgproc.resize(testImage, testImage, new Size(28, 28));
        Mat testImageCropped = cropImage(testImage);
        if (testImageCropped == null) return 0;
        Imgproc.resize(testImageCropped, testImageCropped, new Size(20, 20));
        Mat testImageCroppedPadded = new Mat();
        Imgproc.copyMakeBorder(testImageCropped, testImageCroppedPadded,
                4, 4, 4, 4, Imgproc.BORDER_CONSTANT, new Scalar(0));
        Point shift = ImageProcessingUtil.getBestShift(testImageCroppedPadded);
        Mat shiftedTestImage = ImageProcessingUtil.doShift(testImageCroppedPadded, shift);
        int count = 0;
        float[] pixels = new float[28 * 28];
        for (int i = 0; i < shiftedTestImage.rows(); ++i) {
            for (int j = 0; j < shiftedTestImage.cols(); ++j) {
                pixels[count] = (float) shiftedTestImage.get(i, j)[0];
                count++;
            }
        }
        final List<Classifier.Recognition> results = recognizer.getClassifier().recognizeImage(pixels);
        if (results.size() == 0) {
            return 0;
        }
        Log.d(TAG, "Digit recognized by tensorflow model: " + results.get(0).getTitle());
        return Integer.parseInt(results.get(0).getTitle());
    }

    private Mat cropImage(Mat mat) {
        double width = mat.cols();
        double height = mat.rows();
        // start square is the one 10x10 middle square
        int minWidthHalf = MIN_NUMBER_WIDTH / 2;
        int minHeightHalf = MIN_NUMBER_HEIGHT / 2;
        int leftBound = (int) width / 2 - minWidthHalf;
        int rightBound = (int) width / 2 + minWidthHalf;
        int lowerBound = (int) height / 2 - minHeightHalf;
        int upperBound = (int) height / 2 + minHeightHalf;
        boolean expanded = true;
        while (expanded && lowerBound > 0 && leftBound > 0 && upperBound < 27 && rightBound < 27) {
            expanded = false;
            for (int row = lowerBound; row <= upperBound; ++row) {
                if (mat.get(row, leftBound - 1)[0] > PIXEL_THRESHOLD) {
                    expanded = true;
                    --leftBound;
                    break;
                }
            }
            for (int row = lowerBound; row <= upperBound; ++row) {
                if (mat.get(row, rightBound + 1)[0] > PIXEL_THRESHOLD) {
                    expanded = true;
                    ++rightBound;
                    break;
                }
            }
            for (int col = leftBound; col <= rightBound; ++col) {
                if (mat.get(lowerBound - 1,  col)[0] > PIXEL_THRESHOLD) {
                    expanded = true;
                    --lowerBound;
                    break;
                }
            }
            for (int col = leftBound; col <= rightBound; ++col) {
                if (mat.get(upperBound + 1, col)[0] > PIXEL_THRESHOLD) {
                    expanded = true;
                    ++upperBound;
                    break;
                }
            }
        }
        Log.d("DigitRecognizer", "Vertical: " + lowerBound + " " + upperBound + " Horizontal: " + leftBound + " " + rightBound);
        if ((upperBound - lowerBound) == MIN_NUMBER_HEIGHT && (rightBound - leftBound) == MIN_NUMBER_WIDTH) {
            return null;
        }
        return new Mat(mat, new Rect(leftBound, lowerBound, rightBound - leftBound + 1, upperBound - lowerBound + 1));
    }

    public void solveAutomatically(View view) {
        // todo: solve the sudoku
        startSudokuViewActivity();
    }

    private void startSudokuViewActivity() {
        Intent i = new Intent(this, AutomaticSolverActivity.class);
        Bundle mBundle = new Bundle();
        mBundle.putSerializable("sudokuNumbers", sudokuNumbers);
        i.putExtras(mBundle);
        startActivity(i);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private class ProcessDigitsTask extends AsyncTask<Mat, Void, int[][]> {

        @Override
        protected void onPreExecute() {
            findViewById(R.id.show_sudoku_button).setEnabled(false);
        }

        @Override
        protected int[][] doInBackground(Mat... mats) {
            Mat gridImage = mats[0];
            Mat thresholdedImage = new Mat();
            OpenCvNativeClass.convertGray(gridImage.getNativeObjAddr(), thresholdedImage.getNativeObjAddr());
            /*Imgproc.GaussianBlur(gridImage, gridImage, new Size(9,9), 0);
            Imgproc.adaptiveThreshold(gridImage, thresholdedImage, 255,
                    Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 11, 2);
            Core.bitwise_not(thresholdedImage, thresholdedImage);*/
            TensorFlowDigitRecognizer recognizer = new TensorFlowDigitRecognizer();
            recognizer.initTensorFlowAndLoadModel(getAssets());
            return splitGrid(thresholdedImage, recognizer);
        }

        @Override
        protected void onPostExecute(int[][] sudokuNumbers) {
            findViewById(R.id.show_sudoku_button).setEnabled(true);
            TensorflowSolverActivity.this.sudokuNumbers = sudokuNumbers;
        }
    }
}