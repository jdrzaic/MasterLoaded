package com.example.sudokusolver.imageprocessing;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.example.sudokusolver.edgedetection.EdgeDetectors;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Point3;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;
import org.opencv.utils.Converters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import static org.opencv.imgproc.Imgproc.MORPH_RECT;

/**
 * Created by jelenadrzaic on 15/12/2017.
 */

public class ImageProcessingUtil {

    public static Mat getHoughLines(Mat srcMat) {
        Mat linesClass = new Mat();
        Imgproc.HoughLinesP(srcMat, linesClass, 1, Math.PI/180, 1, srcMat.cols() * 0.5, 100);
        Mat houghLines = new Mat();
        houghLines.create(srcMat.rows(), srcMat.cols(), CvType.CV_8UC1);
        //Drawing lines on the image
        for (int i = 0 ; i < linesClass.cols() ; i++) {
            double[] points = linesClass.get(0, i);
            double x1, y1, x2, y2;
            x1 = points[0];
            y1 = points[1];
            x2 = points[2];
            y2 = points[3];
            org.opencv.core.Point pt1 = new org.opencv.core.Point(x1, y1);
            org.opencv.core.Point pt2 = new org.opencv.core.Point(x2, y2);
            //Drawing lines on an image
            Core.line(houghLines, pt1, pt2,
                    new Scalar(255, 0, 0), 2);
        }
        return houghLines;
    }

    public static Mat detectEdges(Mat srcImage) {
        Mat blurredImage = new Mat();
        Imgproc.GaussianBlur(srcImage, blurredImage, new Size(9,9), 0);
        Mat thresholdedImage = new Mat();
        double upperThreshold = Imgproc.threshold(blurredImage, thresholdedImage, 0, 255,
                Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);
        double lowerThreshold = 0.1 * upperThreshold;
        return EdgeDetectors.Canny(blurredImage, (int) lowerThreshold, (int) upperThreshold);
    }

    public static void displayImage(Mat dispImage, ImageView imageView) {
        Bitmap bitMap = Bitmap.createBitmap(
                dispImage.cols(), dispImage.rows(), Bitmap.Config.RGB_565);
        Utils.matToBitmap(dispImage, bitMap);
        imageView.setImageBitmap(bitMap);
    }

    public static Mat connectComponents(Mat srcImage) {
        Size kernelSize = new Size(5, 5);
        Mat element = Imgproc.getStructuringElement(MORPH_RECT, kernelSize,
                new org.opencv.core.Point(1,1));
        Mat destImage = new Mat();
        Imgproc.morphologyEx(srcImage, destImage, Imgproc.MORPH_CLOSE, element);
        return destImage;
    }

    public static MatOfPoint2f approximateContour(MatOfPoint contour, boolean closed) {
        MatOfPoint2f sampledImageCurve = new MatOfPoint2f(contour.toArray());
        double contourLength = Imgproc.arcLength(sampledImageCurve, closed);
        MatOfPoint2f approxImageCurve = new MatOfPoint2f();
        Imgproc.approxPolyDP(
                sampledImageCurve, approxImageCurve, 0.05 * contourLength, true);
        return approxImageCurve;
    }

    public static void drawApproxContour(Mat destImage, MatOfPoint2f contourCurve) {
        ArrayList<MatOfPoint> approxContours = new ArrayList<>();
        MatOfPoint approxCurveConverted = new MatOfPoint(contourCurve.toArray());
        approxContours.add(approxCurveConverted);
        Imgproc.drawContours(
                destImage, approxContours, 0, new Scalar(220,167,102), 3);

    }

    public static Mat adjustPerspective(MatOfPoint2f approxImageCurve, Mat srcImage) {
        org.opencv.core.Point rectPoints[] = approxImageCurve.toArray();
        int correctedImageDim = srcImage.rows() < srcImage.cols() ? srcImage.rows() : srcImage.cols();
        Mat cutImage = new Mat(correctedImageDim, correctedImageDim, srcImage.type());
        Arrays.sort(rectPoints, new Comparator<Point>() {
            @Override
            public int compare(org.opencv.core.Point point, org.opencv.core.Point t1) {
                if (point.x + point.y == t1.x + t1.y) {
                    return 0;
                }
                return point.x + point.y - t1.x - t1.y > 0 ? 1 : -1;
            }
        });
        org.opencv.core.Point topLeft = rectPoints[0];
        org.opencv.core.Point bottomRight = rectPoints[3];
        Arrays.sort(rectPoints, new Comparator<org.opencv.core.Point>() {
            @Override
            public int compare(org.opencv.core.Point point, org.opencv.core.Point t1) {
                if (point.y - point.x == t1.y - t1.x) {
                    return 0;
                }
                return point.y - point.x - t1.y + t1.x > 0 ? 1 : -1;
            }
        });
        org.opencv.core.Point topRight = rectPoints[0];
        org.opencv.core.Point bottomLeft = rectPoints[3];
        // Sort points so that transformation is applied correctly.
        org.opencv.core.Point rectPointsSorted[] = new org.opencv.core.Point[4];
        rectPointsSorted[0] = topLeft;
        rectPointsSorted[1] = topRight;
        rectPointsSorted[2] = bottomRight;
        rectPointsSorted[3] = bottomLeft;
        Mat srcPoints = Converters.vector_Point2f_to_Mat(Arrays.asList(rectPointsSorted));
        Mat destPoints = Converters.vector_Point2f_to_Mat(Arrays.asList(
                new org.opencv.core.Point[] {
                        new org.opencv.core.Point(0, 0),
                        new org.opencv.core.Point(cutImage.cols(),0),
                        new org.opencv.core.Point(cutImage.cols(), cutImage.rows()),
                        new org.opencv.core.Point(0, cutImage.rows())
                }
        ));
        Mat transformation = Imgproc.getPerspectiveTransform(srcPoints, destPoints);
        Imgproc.warpPerspective(srcImage, cutImage, transformation, cutImage.size());
        return cutImage;
    }

    public static double calculateSampledSize(Mat rgbImage, int procWidth, int procHeight) {
        int srcHeight = rgbImage.height();
        int srcWidth = rgbImage.width();
        double inSampleSize = 1.0;
        if (srcHeight <= procHeight && srcWidth <= procWidth) {
            return inSampleSize;
        }
        double heightRatio = procHeight / (double) srcHeight;
        double widthRatio = procWidth / (double) srcWidth;
        // We want both dimensions to be smaller than the matching requested values.
        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        return inSampleSize;
    }

    public static List<MatOfPoint> findContours(Mat srcImage, boolean randColor, boolean drawAll) {
        List<MatOfPoint> contourList = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(srcImage, contourList, hierarchy, Imgproc.RETR_LIST,
                Imgproc.CHAIN_APPROX_SIMPLE);
        Collections.sort(contourList, new Comparator<MatOfPoint>() {
            @Override
            public int compare(MatOfPoint t1, MatOfPoint t2) {
                double areaT1 = Imgproc.contourArea(t1);
                double areaT2 = Imgproc.contourArea(t2);
                return (int) (areaT2 - areaT1);
            }
        });
        Random r = new Random();
        int drawNum;
        if (drawAll) {
            drawNum = contourList.size();
        } else {
            drawNum = 1;
        }
        // Biggest contour - most likely the grid
        return contourList;
    }

    public static Mat drawContour(List<MatOfPoint> contours, int rows, int cols) {
        Mat contoursMat = new Mat();
        contoursMat.create(rows, cols, CvType.CV_8U);
        Scalar color = new Scalar(255,255,255);
        Imgproc.drawContours(contoursMat, contours, 0, color, 4);
        return contoursMat;
    }

    Mat harrisCorner(Mat originalMat) {
        Mat grayMat = originalMat.clone();
        Mat corners = new Mat();
        //Converting the image to grayscale
        Mat tempDst = new Mat();
        //finding corners
        Imgproc.cornerHarris(grayMat, tempDst, 2, 3, 0.04);
        //Normalizing harris corner's output
        Mat tempDstNorm = new Mat();
        Core.normalize(tempDst, tempDstNorm,
                0, 255, Core.NORM_MINMAX);
        Core.convertScaleAbs(tempDstNorm, corners);
        //Drawing corners on a new image
        Random r = new Random();
        for (int i = 0; i < tempDstNorm.cols(); i++) {
            for (int j = 0; j < tempDstNorm.rows(); j++) {
                double[] value = tempDstNorm.get(j, i);
                if (value[0] > 150)
                    Core.circle(corners, new Point(i, j),
                            5, new Scalar(r.nextInt(255)), 2);
            }
        }
        return corners;
    }

    public static Point getBestShift(Mat mat) {
        Moments mm = Imgproc.moments(mat,false);
        Point massCenter = new Point(mm.get_m10()/mm.get_m00() , mm.get_m01()/mm.get_m00());
        int shiftX = (int) Math.round(mat.cols()/2.0 - massCenter.x);
        int shiftY = (int) Math.round(mat.cols()/2.0 - massCenter.y);
        return new Point(shiftX, shiftY);
    }

    public static Mat doShift(Mat mat, Point shift) {
        Mat shiftMat = new Mat(2, 3, CvType.CV_32F);
        double[] data = {1, 0, (int) shift.x, 0, 1, (int) shift.y};
        shiftMat.put(0, 0, data);
        shiftMat.convertTo(shiftMat, CvType.CV_32F);
        Mat shifted = new Mat();
        Imgproc.warpAffine(mat, shifted, shiftMat, new Size(mat.rows(), mat.cols()));
        return shifted;
    }
}
