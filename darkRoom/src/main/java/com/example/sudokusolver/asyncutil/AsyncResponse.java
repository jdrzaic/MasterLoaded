package com.example.sudokusolver.asyncutil;


import org.opencv.core.Mat;

public interface AsyncResponse {
    void processFinish(Mat output);
}
