package com.example.sudokusolver;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.sudokusolver.gameutil.GameEngine;
import com.example.sudokusolver.gameutil.SudokuChecker;
import com.example.sudokusolver.gameutil.SudokuGenerator;
import com.example.sudokusolver.gameutil.SudokuSolver;

import org.opencv.gpu.Gpu;

import es.dmoral.toasty.Toasty;

public class AutomaticSolverActivity extends AppCompatActivity {

    private int[][] sudokuNumbers;
    private int[][] solvedSudokuNumbers;
    boolean solvable = false;

    public void setSolvable(boolean solvable) {
        this.solvable = solvable;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_automatic_solver);
        Object[] objectArray = (Object[]) getIntent().getExtras().getSerializable("sudokuNumbers");
        if (objectArray != null) {
            sudokuNumbers = new int[objectArray.length][];
            for (int i = 0; i < objectArray.length; i++) {
                sudokuNumbers[i] = (int[]) objectArray[i];
            }
        }
        GameEngine.getNewInstance().createGrid(this, sudokuNumbers);
    }

    private int[][] copyGridValues() {
        int[][] sudokuNumbers = GameEngine.getInstance().getGrid().getRawValues();
        int[][] sudokuNumbersDeepCopy = new int[9][9];
        for (int i = 0; i < 9; ++i) {
            for (int j = 0; j < 9; ++j) {
                sudokuNumbersDeepCopy[i][j] = sudokuNumbers[i][j];
            }
        }
        return sudokuNumbersDeepCopy;
    }

    public void solveAutomatically(View view) {
        if (!solvable) {
            Toasty.error(this, "Sudoku is not valid, please check the input and try again!", Toast.LENGTH_LONG).show();
        }
        solvable = true;
        sudokuNumbers = copyGridValues();
        int[][] sudokuNumbersDeepCopy = new int[9][9];
        for (int i = 0; i < 9; ++i) {
            for (int j = 0; j < 9; ++j) {
                sudokuNumbersDeepCopy[i][j] = sudokuNumbers[i][j];
            }
        }
        if (!SudokuSolver.solve(0, 0, sudokuNumbersDeepCopy)) {
            Toasty.error(this, "Sudoku is not valid, please check the input and try again!", Toast.LENGTH_LONG).show();
        } else {
            GameEngine.getInstance().getNumGrid().setDisabled();
            for (int i = 0; i < 9; ++i) {
                for (int j = 0; j < 9; ++j) {
                    // Numbers that were computed by a sudoku solver
                    if (sudokuNumbers[i][j] <= 0) {
                        GameEngine.getInstance().setSelectedPosition(i, j);
                        GameEngine.getInstance().setNumber(sudokuNumbersDeepCopy[i][j]);
                    }
                }
            }
        }
        GameEngine.getInstance().setSelectedPosition(-1, -1);
    }

    public void showHint(View view) {
        if (!solvable) {
            Toasty.error(this, "Sudoku is not valid, please check the input and try again!", Toast.LENGTH_SHORT).show();
        }
        solvable = true;
        sudokuNumbers = copyGridValues();
        int[][] sudokuNumbersDeepCopy = new int[9][9];
        for (int i = 0; i < 9; ++i) {
            for (int j = 0; j < 9; ++j) {
                sudokuNumbersDeepCopy[i][j] = sudokuNumbers[i][j];
            }
        }
        GameEngine engine = GameEngine.getInstance();
        if (!SudokuSolver.solve(0, 0, sudokuNumbersDeepCopy)) {
            Toasty.error(this, "Sudoku is not valid, please check the input and try again!", Toast.LENGTH_LONG).show();
        } else if (engine.getSelectedPosition().x > -1 && engine.getSelectedPosition().y > -1) {
            engine.setNumber(sudokuNumbersDeepCopy[(int)engine.getSelectedPosition().x][(int)engine.getSelectedPosition().y]);
        } else {
            for (int i = 0; i < 9; ++i) {
                boolean found = false;
                for (int j = 0; j < 9; ++j) {
                    // Numbers that were computed by a sudoku solver
                    if (sudokuNumbers[i][j] <= 0) {
                        GameEngine.getInstance().setSelectedPosition(i, j);
                        GameEngine.getInstance().setNumber(sudokuNumbersDeepCopy[i][j]);
                        found = true;
                        break;
                    }
                }
                if (found) break;
            }
        }
        GameEngine.getInstance().setSelectedPosition(-1, -1);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(AutomaticSolverActivity.this, IODarkRoom.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new SolveSudokuTask().execute();

    }

    private class SolveSudokuTask extends AsyncTask<Void, Boolean, Boolean> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if (sudokuNumbers == null || solvedSudokuNumbers != null) {
                return true;
            }
            solvedSudokuNumbers = new int[9][9];
            for (int i = 0; i < 9; ++i) {
                for (int j = 0; j < 9; ++j) {
                    solvedSudokuNumbers[i][j] = sudokuNumbers[i][j];
                }
            }
            solvable = SudokuSolver.solve(0, 0, solvedSudokuNumbers);
            return solvable;
        }

        @Override
        protected void onPostExecute(Boolean solved) {
            if (!solved) {
                Toasty.error(AutomaticSolverActivity.this, "Sudoku is not valid, please check the input and try again!", Toast.LENGTH_LONG).show();
            }
        }

    }
}