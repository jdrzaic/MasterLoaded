package com.example.sudokusolver.edgedetection;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class EdgeDetectors {

    public static Mat Canny(Mat originalMat, int lowerThreshold, int upperThreshold) {
        Mat cannyEdges = new Mat();
        //Converting the image to grayscale
        Imgproc.Canny(originalMat, cannyEdges, lowerThreshold, upperThreshold);
        return cannyEdges;
    }
}
