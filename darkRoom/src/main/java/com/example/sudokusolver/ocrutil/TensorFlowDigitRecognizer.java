/*
 *    Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.example.sudokusolver.ocrutil;

import android.content.res.AssetManager;
import android.graphics.PointF;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.example.sudokusolver.ocrutil.tensorflow.Classifier;
import com.example.sudokusolver.ocrutil.tensorflow.TensorFlowImageClassifier;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


public class TensorFlowDigitRecognizer {

    private static final String TAG = "TFDigitRecognizer";

    private static final int INPUT_SIZE = 28;
    private static final String INPUT_NAME = "input";
    private static final String OUTPUT_NAME = "output";

    private static final String MODEL_FILE = "file:///android_asset/expert-graph.pb";
    private static final String LABEL_FILE =
            "file:///android_asset/graph_label_strings.txt";

    private Classifier classifier;
    private Executor executor = Executors.newSingleThreadExecutor();

    public void initTensorFlowAndLoadModel(final AssetManager manager) {
        /*executor.execute(new Runnable() {
            @Override
            public void run() {*/
                try {
                    classifier = TensorFlowImageClassifier.create(
                            manager,
                            MODEL_FILE,
                            LABEL_FILE,
                            INPUT_SIZE,
                            INPUT_NAME,
                            OUTPUT_NAME);
                    Log.d(TAG, "Load Success");
                } catch (final Exception e) {
                    Log.e(TAG, "Exception tensorflow: " + e.toString());
                    throw new RuntimeException("Error initializing TensorFlow!", e);
                }
            /*}
        });*/
    }

    public Classifier getClassifier() {
        return classifier;
    }

    public void closeTensorflow() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                classifier.close();
            }
        });
    }
}