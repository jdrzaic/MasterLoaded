package com.example.sudokusolver.ocrutil;

import android.util.Log;

import com.example.sudokusolver.SolverActivity;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.ml.CvKNearest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by jelenadrzaic on 29/12/2017.
 */

public class DigitRecognizer {

    private int totalImages;
    private int width;
    private int height;
    private CvKNearest knn;
    private static int MIN_NUMBER_WIDTH = 10;
    private static int MIN_NUMBER_HEIGHT = 10;
    private static int PIXEL_THRESHOLD = 50;

    public void readMNISTData(String imagesPath, String labelsPath) {
        File mnistImagesFile = new File(imagesPath);
        Mat trainingImages = null;
        try {
            FileInputStream imagesReader = new FileInputStream(mnistImagesFile);
            byte[] header = new byte[16];
            imagesReader.read(header, 0, 16);
            ByteBuffer tmp = ByteBuffer.wrap(header, 4, 12);
            totalImages = tmp.getInt();
            width = tmp.getInt();
            height = tmp.getInt();
            int pxCount = width * height;
            trainingImages = new Mat(totalImages, pxCount, CvType.CV_8U);
            for (int i = 0; i < totalImages; i++) {
                byte[] image = new byte[pxCount];
                imagesReader.read(image, 0, pxCount);
                trainingImages.put(i, 0, image);
            }
            trainingImages.convertTo(trainingImages, CvType.CV_32FC1);
            imagesReader.close();
        } catch (FileNotFoundException ex) {
            Log.e("DigitRecognizer", "File not found: " + ex.toString());
        } catch (IOException ex) {
            Log.e("DigitRecognizer", "Unable to read file: " + ex.toString());
        }
        Mat trainingLabels = null;
        byte[] labelsData = new byte[totalImages];
        File mnistLabelsFile = new File(labelsPath);
        try {
            FileInputStream labelsReader = new FileInputStream(mnistLabelsFile);
            trainingLabels = new Mat(totalImages, 1, CvType.CV_8U);
            Mat tempLabels = new Mat(1, totalImages, CvType.CV_8U);
            byte[] header = new byte[8];
            labelsReader.read(header, 0, 8);
            labelsReader.read(labelsData, 0, totalImages);
            tempLabels.put(0, 0, labelsData);
            Core.transpose(tempLabels, trainingLabels);
            trainingLabels.convertTo(trainingLabels, CvType.CV_32FC1);
            labelsReader.close();

        } catch (FileNotFoundException ex) {
            Log.e("DigitRecognizer", "File not found: " + ex.toString());
        } catch (IOException ex) {
            Log.e("DigitRecognizer", "Unable to read file: " + ex.toString());
        }
        knn = new CvKNearest();
        knn.train(trainingImages, trainingLabels, new Mat(), false, 20, false);
        try {
            trainingImages.release();
            trainingLabels.release();
        } catch (NullPointerException ex) {}
    }

    public double findMatch(Mat testImage, SolverActivity solver) {
        Imgproc.resize(testImage, testImage, new Size(width, height));
        testImage = cropImage(testImage);
        if (testImage == null) return 0;
        Imgproc.resize(testImage, testImage, new Size(28, 28));
        Mat test = new Mat(1, testImage.rows() * testImage.cols(), CvType.CV_32FC1);
        int count = 0;
        for (int i = 0; i < testImage.rows(); ++i) {
            for (int j = 0; j < testImage.cols(); ++j) {
                test.put(0, count, testImage.get(i, j)[0]);
                count++;
            }
        }
        Mat results = new Mat(1, 1, CvType.CV_8U);
        knn.find_nearest(test, 20, results, new Mat(), new Mat());
        return results.get(0, 0)[0];
    }

    private Mat cropImage(Mat mat) {
        double width = mat.cols();
        double height = mat.rows();
        // start square is the one 10x10 middle square
        int minWidthHalf = MIN_NUMBER_WIDTH / 2;
        int minHeightHalf = MIN_NUMBER_HEIGHT / 2;
        int leftBound = (int) width / 2 - minWidthHalf;
        int rightBound = (int) width / 2 + minWidthHalf;
        int lowerBound = (int) height / 2 - minHeightHalf;
        int upperBound = (int) height / 2 + minHeightHalf;
        boolean expanded = true;
        while (expanded && lowerBound > 0 && leftBound > 0 && upperBound < 27 && rightBound < 27) {
            expanded = false;
            for (int row = lowerBound; row <= upperBound; ++row) {
                if (mat.get(row, leftBound - 1)[0] > PIXEL_THRESHOLD) {
                    expanded = true;
                    --leftBound;
                    break;
                }
            }
            for (int row = lowerBound; row <= upperBound; ++row) {
                if (mat.get(row, rightBound + 1)[0] > PIXEL_THRESHOLD) {
                    expanded = true;
                    ++rightBound;
                    break;
                }
            }
            for (int col = leftBound; col <= rightBound; ++col) {
                if (mat.get(lowerBound - 1,  col)[0] > PIXEL_THRESHOLD) {
                    expanded = true;
                    --lowerBound;
                    break;
                }
            }
            for (int col = leftBound; col <= rightBound; ++col) {
                if (mat.get(upperBound + 1, col)[0] > PIXEL_THRESHOLD) {
                    expanded = true;
                    ++upperBound;
                    break;
                }
            }
        }
        Log.d("DigitRecognizer", "Vertical: " + lowerBound + " " + upperBound + " Horizontal: " + leftBound + " " + rightBound);
        if ((upperBound - lowerBound) == MIN_NUMBER_HEIGHT && (rightBound - leftBound) == MIN_NUMBER_WIDTH) {
            return null;
        }
        return new Mat(mat, new Rect(leftBound, lowerBound, rightBound - leftBound + 1, upperBound - lowerBound + 1));
    }
}
