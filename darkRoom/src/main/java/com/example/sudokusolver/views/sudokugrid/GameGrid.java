package com.example.sudokusolver.views.sudokugrid;

import android.content.Context;
import android.widget.Toast;

import com.example.sudokusolver.gameutil.SudokuChecker;
import com.example.sudokusolver.gameutil.SudokuGenerator;

import es.dmoral.toasty.Toasty;


public class GameGrid {
    private SudokuCell[][] Sudoku = new SudokuCell[9][9];

    private Context context;

    public GameGrid( Context context ){
        this.context = context;
        for( int x = 0 ; x < 9 ; x++ ){
            for( int y = 0 ; y < 9 ; y++){
                Sudoku[x][y] = new SudokuCell(context);
            }
        }
    }

    public void setGrid(int[][] grid) {
        for(int x = 0 ; x < 9 ; ++x) {
            for( int y = 0 ; y < 9 ; ++y) {
                Sudoku[x][y].setInitValue(grid[x][y]);
            }
        }
    }

    public SudokuCell[][] getGrid(){
        return Sudoku;
    }

    public SudokuCell getItem(int x , int y ){
        return Sudoku[x][y];
    }

    public SudokuCell getItem( int position ){
        int x = position % 9;
        int y = position / 9;

        return Sudoku[x][y];
    }

    public void setItem( int x , int y , int number ){
        int [][] sudGrid = new int[9][9];
        for( int xt = 0 ; xt < 9 ; xt++ ){
            for( int yt = 0 ; yt < 9 ; yt++ ){
                sudGrid[xt][yt] = getItem(xt,yt).getValue();
            }
        }
        Sudoku[x][y].setValue(number);
        if (checkGame()) {
            Toasty.success(context, "Sudoku is now solved!", Toast.LENGTH_LONG).show();
        }
    }

    public int[][] getRawValues() {
        int [][] sudGrid = new int[9][9];
        for( int x = 0 ; x < 9 ; x++ ){
            for( int y = 0 ; y < 9 ; y++ ){
                sudGrid[x][y] = getItem(x,y).getValue();
            }
        }
        return sudGrid;
    }

    private boolean checkGame(){
        int [][] sudGrid = new int[9][9];
        for( int x = 0 ; x < 9 ; x++ ){
            for( int y = 0 ; y < 9 ; y++ ){
                sudGrid[x][y] = getItem(x,y).getValue();
            }
        }
        return SudokuChecker.getInstance().checkSudoku(sudGrid);
    }
}