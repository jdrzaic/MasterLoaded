package com.example.sudokusolver.views.buttonsgrid;

import android.view.View;

/**
 * Created by jelenadrzaic on 08/01/2018.
 */

public class NumbersGrid {

    private View[] numberButtons;

    public NumbersGrid(View[] numberButtons) {
        this.numberButtons = numberButtons;
    }

    public void setDisabled(int number) {
        if (numberButtons[number - 1] == null) return;
        if (number < 1 || number > 9) return;
        numberButtons[number - 1].setEnabled(false);
        numberButtons[number - 1].invalidate();
    }

    public void resetDisabled() {
        for (int i = 0; i < 9; ++i) {
            if (numberButtons[i] == null) continue;
            numberButtons[i].setEnabled(true);
            numberButtons[i].invalidate();
        }
    }

    public void setDisabled() {
        for (int i = 0; i < 9; ++i) {
            if (numberButtons[i] == null) continue;
            numberButtons[i].setEnabled(false);
            numberButtons[i].invalidate();
        }
    }

    void setButtonNumber(int number, View button) {
        if (numberButtons[number - 1] != null) return;
        numberButtons[number - 1] = button;
    }
}
