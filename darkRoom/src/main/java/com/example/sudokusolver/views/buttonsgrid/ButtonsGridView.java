package com.example.sudokusolver.views.buttonsgrid;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.example.sudokusolver.R;
import com.example.sudokusolver.gameutil.GameEngine;

public class ButtonsGridView extends GridView{

    public ButtonsGridView( Context context , AttributeSet attrs ){
        super(context , attrs);

        ButtonsGridViewAdapter gridViewAdapter = new ButtonsGridViewAdapter(context);

        setAdapter(gridViewAdapter);
    }

    class ButtonsGridViewAdapter extends BaseAdapter {

        private Context context;

        ButtonsGridViewAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return 9;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position + 1;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            if( v == null){
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                v = inflater.inflate(R.layout.number_button, parent, false);

                NumberButton btn;
                btn = (NumberButton)v;
                btn.setTextSize(15);
                btn.setTextColor(Color.WHITE);
                btn.setId(position + 1);

                if (position < 9){
                    btn.setText(String.valueOf(position + 1));
                    btn.setNumber(position + 1);
                    GameEngine.getInstance().getNumGrid().setButtonNumber(position + 1, v);
                }else if (position == 10) {
                    btn.setText("DELETE");
                    btn.setNumber(0);
                } else {
                    btn.setText(" ");
                    btn.setNumber(-1);
                }
                return btn;
            }

            return v;
        }

    }
}