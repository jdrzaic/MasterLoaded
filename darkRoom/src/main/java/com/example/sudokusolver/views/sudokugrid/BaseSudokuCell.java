package com.example.sudokusolver.views.sudokugrid;

import android.content.Context;
import android.view.View;


public class BaseSudokuCell extends View {

    private int value;
    private boolean modifiable = true;
    protected boolean initial = true;

    public BaseSudokuCell(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    public void setInitValue(int value){
        this.value = value;
        invalidate();
    }

    public void setValue( int value ){
        if (modifiable) {
            this.initial = false;
            this.value = value;
        }
        invalidate();
    }

    public int getValue(){
        return value;
    }
}