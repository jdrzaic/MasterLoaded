package com.example.sudokusolver.camerautil;

import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.util.Log;

import org.opencv.android.JavaCameraView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by jelenadrzaic on 15/12/2017.
 */

public class CamView extends JavaCameraView implements Camera.PictureCallback {

    public CamView(Context context, AttributeSet attrs) {
        super(context, attrs);
        //setFrameSize();
    }

    @Override
    public void onPictureTaken(byte[] bytes, Camera camera) {
        mCamera.startPreview();
        mCamera.setPreviewCallback(this);
    }

    public void takePicture() {
        mCamera.setPreviewCallback(null);
        mCamera.takePicture(null, null, this);
    }

    private void setFrameSize() {

        try {
            // set preview size and make any resize, rotate or
            // reformatting changes here
            Camera.Parameters parameters = mCamera.getParameters();
            List<Camera.Size> supportedSizes = parameters.getSupportedPreviewSizes();
            Collections.sort(supportedSizes, new Comparator<Camera.Size>() {
                @Override
                public int compare(Camera.Size size, Camera.Size t1) {
                    return size.width - t1.width;
                }
            });
            Camera.Size finSize = null;
            for (Camera.Size size : supportedSizes) {
                if (size.width >= 600) {
                    parameters.setPreviewSize(size.width, size.height);
                    parameters.setPictureSize(size.width, size.height);
                    finSize = size;
                    break;
                }
            }
            // Set parameters for camera

            mCamera.setParameters(parameters);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
