package com.example.sudokusolver.camerautil;

/**
 * Created by jelenadrzaic on 11/01/2018.
 */

public class NativeFrameClass {
    public native static int processFrame(long mAddrOrig, long mAddrConn);
}
