package com.example.sudokusolver.camerautil;

/**
 * Created by jelenadrzaic on 09/01/2018.
 */

public class OpenCvNativeClass {
    public native static int convertGray(long matAddrRgba, long matAddrGray);
}
