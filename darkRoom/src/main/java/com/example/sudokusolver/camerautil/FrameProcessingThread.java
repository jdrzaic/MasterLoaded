package com.example.sudokusolver.camerautil;

import android.graphics.Point;
import android.view.Display;

import com.example.sudokusolver.imageprocessing.ImageProcessingUtil;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by jelenadrzaic on 04/01/2018.
 */

public class FrameProcessingThread extends Thread {

    private Mat contouredImage;
    private Mat grayImage;
    private int frameCounter;
    private MatOfPoint2f approxContour;
    private ConcurrentLinkedQueue<Mat> mProducerQueue;
    private ConcurrentLinkedQueue<Mat> mConsumerQueue;
    private int imageWidth;
    private int imageHeight;

    public FrameProcessingThread(ConcurrentLinkedQueue<Mat> mProducerQueue,
                                 ConcurrentLinkedQueue<Mat> mConsumerQueue, Mat contouredImage,
                                 Mat grayImage, int frameCounter, MatOfPoint2f approxContour,
                                 int imageWidth, int imageHeight) {
        this.mProducerQueue = mProducerQueue;
        this.mConsumerQueue = mConsumerQueue;
        this.contouredImage = contouredImage;
        this.grayImage = grayImage;
        this.frameCounter = frameCounter;
        this.approxContour = approxContour;
        this.imageHeight = imageHeight;
        this.imageWidth = imageWidth;
    }

    @Override
    public void run() {
        super.run();
        while (true) {
            if (mProducerQueue.size() == 0) continue;
            Mat originalImage = mProducerQueue.poll();
            contouredImage = originalImage.clone();
            ++frameCounter;
            if (frameCounter % 50 != 0 && approxContour != null) {
                ImageProcessingUtil.drawApproxContour(contouredImage, approxContour);
                mConsumerQueue.add(contouredImage);
                continue;
            }
            grayImage = new Mat();
            Imgproc.cvtColor(originalImage, originalImage, Imgproc.COLOR_BGR2RGB);
            Imgproc.cvtColor(originalImage, grayImage, Imgproc.COLOR_BGR2GRAY);
            double downSampleRatio = ImageProcessingUtil.calculateSampledSize(
                    grayImage, imageWidth, imageHeight);
            Imgproc.resize(grayImage, grayImage, new Size(),
                    downSampleRatio, downSampleRatio, Imgproc.INTER_AREA);
            Mat edgesImage = ImageProcessingUtil.detectEdges(grayImage);
            Mat connectedEdgesImage = ImageProcessingUtil.connectComponents(edgesImage);
            // Get biggest contour
            List<MatOfPoint> contours = ImageProcessingUtil.findContours(connectedEdgesImage, false,
                    false);
            // continue
            if (contours.size() == 0) {
                mConsumerQueue.add(originalImage);
                continue;
            }
            MatOfPoint outerContour = contours.get(0);
            MatOfPoint2f approxContourNew = ImageProcessingUtil.approximateContour(outerContour, true);
            if (approxContourNew.total() >= 4) {
                approxContour = approxContourNew;
            }
            if (approxContour == null) {
                mConsumerQueue.add(originalImage);
                continue;
            }
            ImageProcessingUtil.drawApproxContour(contouredImage, approxContour);
            mConsumerQueue.add(contouredImage);
        }
    }
}
