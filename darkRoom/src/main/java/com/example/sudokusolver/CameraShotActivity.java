package com.example.sudokusolver;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;

import com.example.sudokusolver.camerautil.CamView;
import com.example.sudokusolver.camerautil.IntentHelper;
import com.example.sudokusolver.camerautil.OpenCvNativeClass;
import com.example.sudokusolver.imageprocessing.ImageProcessingUtil;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.imgproc.Imgproc;

import java.util.List;

public class CameraShotActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static String TAG = "DarkRoom::CameraShot";
    private CamView camView;
    Mat contouredImage;
    Mat grayImage;
    Mat gridImagePart;
    private int frameCounter = 0;
    MatOfPoint2f approxContour;
    public boolean safeToTakePicture = false;

    static {
        System.loadLibrary("MyLibs");
    }

    @Override
    public void setRequestedOrientation(int requestedOrientation) {
        super.setRequestedOrientation(requestedOrientation);
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully.");
                    camView.enableView();
                }
                break;
                default:
                {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_camera_shot);
        camView = findViewById(R.id.camera_shot_activity_surface_view);
        camView.setCameraIndex(0);
        camView.setVisibility(SurfaceView.VISIBLE);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int imageWidth = size.x;
        int imageHeight = size.y;
        double ratioWidth = 1;
        while (imageWidth / ratioWidth > 1000) {
            ratioWidth *= 1.5;
        }
        double ratioHeight = 1;
        while (imageHeight / ratioHeight > 1000) {
            ratioHeight *= 1.5;
        }
        double ratio = ratioHeight > ratioWidth ? ratioHeight : ratioWidth;
        Log.d(TAG, "Width: " + imageWidth / ratio + " Height: " + imageHeight / ratio);
        camView.setMaxFrameSize((int) (imageWidth / ratio), (int)(imageHeight / ratio));
        camView.setCvCameraViewListener(this);
        ImageButton cameraButton = findViewById(R.id.take_photo_button);
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (safeToTakePicture) {
                    camView.takePicture();
                    gridImagePart = ImageProcessingUtil.adjustPerspective(approxContour, grayImage);
                    IntentHelper.addObjectForKey(contouredImage, "originalImage");
                    IntentHelper.addObjectForKey(gridImagePart, "extractedGrid");
                    safeToTakePicture = false;
                    Intent intent = new Intent(CameraShotActivity.this, TensorflowSolverActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onCameraViewStopped() {
        if (gridImagePart != null) {
            gridImagePart.release();
        }
        if (contouredImage != null) {
            contouredImage.release();
        }
        if (approxContour != null) {
            approxContour.release();
        }
        if (grayImage != null) {
            grayImage.release();
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        safeToTakePicture = true;
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        try {
            Mat originalImage = inputFrame.rgba();
            contouredImage = originalImage.clone();
            ++frameCounter;
            if (frameCounter % 60 != 0 && approxContour != null) {
                ImageProcessingUtil.drawApproxContour(contouredImage, approxContour);
                return contouredImage;
            }
            grayImage = new Mat();
            Imgproc.cvtColor(originalImage, originalImage, Imgproc.COLOR_BGR2RGB);
            Imgproc.cvtColor(originalImage, grayImage, Imgproc.COLOR_RGB2GRAY);
            Mat edgesImage = ImageProcessingUtil.detectEdges(grayImage);
            Mat connectedEdgesImage = ImageProcessingUtil.connectComponents(edgesImage);
            // Get biggest contour
            List<MatOfPoint> contours = ImageProcessingUtil.findContours(connectedEdgesImage, false,
                    false);
            // continue
            if (contours.size() == 0) {
                return inputFrame.rgba();
            }
            MatOfPoint outerContour = contours.get(0);
            MatOfPoint2f approxContourNew = ImageProcessingUtil.approximateContour(outerContour, true);
            if (approxContourNew.total() == 4) {
                approxContour = approxContourNew;
            }
            if (approxContour == null) {
                return inputFrame.rgba();
            }
            ImageProcessingUtil.drawApproxContour(contouredImage, approxContour);
            return contouredImage;
        } catch (Exception ex) {
            Log.d(TAG, "Unable to process frame: " + ex.toString());
            return inputFrame.rgba();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_8, this, mLoaderCallback);
    }
}